package com.yiguang.algorithm.collection;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;

import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;

//题目：78、
public class Solution {
	public static void main(String[] args) {
		System.out.println(JSON.toJSON(subsets(new int[] {1,2,3})));
		System.out.println(011&001);
		LocalDateTime l1 = LocalDateTime.of(2021, 8, 31, 13, 30);
		LocalDateTime l2 = l1.plusMonths(1);
		System.out.println(l1);
		System.out.println(l2);
		System.out.println(l2.plusMonths(1));
		LocalDateTime now = LocalDateTime.now();
		System.out.println(now.plusMonths(3).withHour(0).withMinute(0).withSecond(0).minusDays(1L));
		System.out.println("01".startsWith("0")?"01".substring(1, "01".length()):"01");
		
		
//		Map<String, String> header = new HashMap();
//		header.put("Content-Type","application/json");
//		Map<String, Object> params = new HashMap();
//		params.put("username", "rntd");
//		params.put("password", "Admin_1234");
//		params.put("identification", "88.205.101");
//		System.out.println(HttpRequest.post("https://gate.gongyeid.com/api/ident/pro/IdentificationAnalysis").
//				addHeaders(header).body(JSON.toJSONString(params)).execute().body());
		
		
		Map<String, String> header = new HashMap();
		header.put("Content-Type","application/json");
		Map<String, Object> params = new HashMap();
		params.put("username", "rntd");
		params.put("password", "Admin_1234");
		params.put("identification", "88.205.101");
		
//		System.out.println(HttpRequest.post("https://gate.gongyeid.com/api/ident/pro/IdentificationAnalysis").
//				addHeaders(header).body(JSON.toJSONString(params)).execute().body());
		
		System.out.println(HttpRequest.post("https://icode.asun.cloud/openApi/v1/agent/login").
				addHeaders(header).body(JSON.toJSONString(params)).execute().body());

	}

	/*
	 * 78. 子集 
	 * 给你一个整数数组 nums ，数组中的元素互不相同 。返回该数组所有可能的子集（幂集）。 
	 * 解集 不能 包含重复的子集。你可以按 任意顺序
	 * 返回解集。
	 */
	public static List<List<Integer>> subsets(int[] nums) {
		List<Integer> t = new ArrayList<Integer>();
	    List<List<Integer>> ans = new ArrayList<List<Integer>>();
        int n = nums.length;
        for (int mask = 0; mask < (1 << n); ++mask) {
            t.clear();
            // 000 001 010 011 100 101 110 111
            for (int i = 0; i < n; ++i) {
                if ((mask & (1 << i)) != 0) {// 1 001,2 010,4 100    
                    t.add(nums[i]);
                }
            }
            ans.add(new ArrayList<Integer>(t));
        }
        return ans;
    }
	
	/*
	 * 90. 子集 II 
	 * 给你一个整数数组 nums ，其中可能包含重复元素，请你返回该数组所有可能的子集（幂集）。 
	 * 解集 不能包含重复的子集。返回的解集中，子集可以按 任意顺序 排列。
	 */
	public List<List<Integer>> subsetsWithDup(int[] nums) {
		List result = new ArrayList();
		return result;
    }
	
	
}
