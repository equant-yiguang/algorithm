package com.yiguang.algorithm.dp;

/*
 * 463. 岛屿的周长
 */
public class IslandGirth {

	public int getIslandMaxArea(int[][] grid) {
		int result = 0;
		for(int i=0; i<grid.length; i++) {
			for(int j=0; j<grid[0].length; j++) {
				if(grid[i][j]==1) {
					// top 
					if((i-1>=0 && grid[i-1][j]==0) || i-1<0) {
						result++;
					}
					// right
					if((j+1<grid[0].length && grid[i][j+1]==0) || j+1>=grid[0].length) {
						result++;
					}
					// bottom
					if((i+1<grid.length && grid[i+1][j]==0) || i+1>=grid.length) {
						result++;
					}
					// left
					if((j-1>=0 && grid[i][j-1]==0) || j-1<0) {
						result++;
					}
				}
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		/*
		 * 11000
		 * 11000
		 * 00100
		 * 00011
		 */
		int[][] grid = {{0,0,1,0,0,0,0,1,0,0,0,0,0},{0,0,0,0,0,0,0,1,1,1,0,0,0},{0,1,1,0,1,0,0,0,0,0,0,0,0},{0,1,0,0,1,1,0,0,1,0,1,0,0},{0,1,0,0,1,1,0,0,1,1,1,0,0},{0,0,0,0,0,0,0,0,0,0,1,0,0},{0,0,0,0,0,0,0,1,1,1,0,0,0},{0,0,0,0,0,0,0,1,1,0,0,0,0}};
		IslandGirth islandNumProblem = new IslandGirth();
		System.out.println(islandNumProblem.getIslandMaxArea(grid));
	}

}
