package com.yiguang.algorithm.dp;

public class Summary {
	//关于dp
	/*
	 递归算法、回溯算法(深度遍历dfs思想)、记忆化搜索思想、dp算法（一维dp、二维dp）
	 线性DP；
	 区间DP；
	 背包DP；
	 树形DP；
	 状态压缩DP；
	 数位DP；
	 计数型DP；
	 递推型DP；
	 概率型DP；
	 博弈型DP；
	 记忆化搜索；
	 
	 字符串的最值问题80%都是动态规划。包括
	 最长回文子串
	 最长回文序列(本题)
	 最长公共前缀
	 */
}
