package com.yiguang.algorithm.other;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**  
 * @Title: SaveWater.java
 * @Package com.yiguang.algorithm.other
 * @ClassName: SaveWater
 * @Description: 该问题未能解决
 * @author yiguang
 * @date 2021/11/18
 */ 
public class SaveWater {
	public List<List<Integer>> solveNQueens(int n) {
		List<List<Integer>> result = new ArrayList();
//		List<Integer> middle = new 
//		dolt(result, )
		return result;
	}
	
	// 遍历开始
	// 1.先确认能否存水（不能（做坝），能，待确认）、再确认最小坝值
	static Map<Integer, Map<Integer, Integer>> canSave = new HashMap();
	static Map<Integer, Map<Integer, Integer>> noSave = new HashMap();
	static Map<Integer, Map<Integer, Integer>> mabeySave = new HashMap();
	//static Map<Integer, Map<Integer, Boolean>> fenceAll = new HashMap();
	public int sumSaveWater(int[][] n) {
//		List<Integer> problem = new ArrayList();
//		List<Integer> minValue = new ArrayList();
		
		int hasProblemNum=0;
		for(int y=1; y<n.length-1; y++) {
			for(int x=1; x<n[y].length-1; x++) {
				//存水状况
				isClockwiseSaveWater(n, y,x);
//				int isSaveWater =  isClockwiseSaveWater(n, y,x);
//				if(isSaveWater==1) { // 能存水
//					Map<Integer, Integer> positive = new HashMap();
//					positive.put(y, min);
//					noProblemValue.put(x, positive);
//				}else if(isSaveWater==2) { // 不能存水
//					n[y][x] = isSaveWater;
//				}else { // 待确认
//					hasProblemNum+=1;
//					Map<Integer, Integer> positive = new HashMap();
//					positive.put(x, isSaveWater);
//					hasProblemValue.put(y, positive);
//				}
			}
		}
		for(int i=n.length-2; i>1; i--) {
			for(int j=n[i].length-2; j>1; j--) {
				int isSaveWater = isAntiClockwiseSaveWater(i,j);
				if(isSaveWater==1) { // 能存水
					Map<Integer, Integer> positive = new HashMap();
					positive.put(j, isSaveWater);
					canSave.put(i, positive);
				}else if(isSaveWater==2) { // 不能存水
					n[i][j] = isSaveWater;
				}
				
			}
		}

		return 1;
	}
	
	private static int isClockwiseSaveWater(int[][] n, int y, int x) {
		int current = n[y][x];
		int left = n[y][x-1];
		int leftTop = n[y-1][x-1];
		int top = n[y-1][x];
		int ritghTop = n[y-1][x+1];
		int right = n[y][x+1];
		int rightBelow = n[y+1][x+1];
		int below = n[y+1][x];
		int leftBelow = n[y+1][x-1];
		int min = left;
		int result = 0;
		if(left>current && leftTop>current && top>current) {
			result = 3;
			if(left > leftTop) {
				min = leftTop;
			}
			if(min > top) {
				min = top;
			}
			if(ritghTop>current && right>current && rightBelow>current && below>current && leftBelow>current) {
				result = 1;
				Map<Integer, Integer> positive = new HashMap();
				positive.put(x, min);
				canSave.put(y, positive);
//				if(min > right) {
//					min = right;
//				}
//				if(min > rightBelow) {
//					min = rightBelow;
//				}
//				if(min > below) {
//					min = below;
//				}
//				if(min > leftBelow) {
//					min = leftBelow;
//				}
			}else {
				Map<Integer, Integer> positive = new HashMap();
				positive.put(x, min);
				mabeySave.put(y, positive);
			}
		}else {
			result = 2;
			min = current;
			Map<Integer, Integer> positive = new HashMap();
			positive.put(x, min);
			noSave.put(y, positive);
		}
		n[y][x]=min;
//		Map<Integer, Boolean> fence = new HashMap();
//		Boolean isSave = true;
//		if(result == 2) {
//			isSave = false;
//		}
//		fence.put(x, isSave);
//		fenceAll.put(y, fence);
		return result;

	}
	
	static int isAntiClockwiseSaveWater(int i, int j) {
		return 1;
//		2;
//		3;
	}
}
